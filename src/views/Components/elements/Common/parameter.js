export const COLOR = [ 
  'green', 
  'yellow', 
  'blue', 
  '#f111c9', 
  '#05f1c7', 
  '#f18905',
  'red', 
  '#4c5761', 
  '#9a7915',
  '#5900f7',
  '#c9da1f',
  '#d04f27',
]