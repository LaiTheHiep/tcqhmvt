import React from 'react';
import { Col, Row, Input, Label, Button, Card, CardBody, CardHeader, CardFooter } from 'reactstrap';
import readXlsxFile from 'read-excel-file';

class SetupBasic extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      parametersMentors: {
        nodes: 80,
        X: 1000,
        Y: 1000,
        W: 2,
        R: 0.3,
        C: 10,
        costLink: 0.4,
        alpha: 0.7,
        Umin: 0.7
      },
      isOpen: false,
    };

    this.toggle = this.toggle.bind(this);
    this.changeText = this.changeText.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  changeText(event){
    switch (event.target.name) {
      case 'nodes':
        this.state.parametersMentors.nodes = Number(event.target.value);
        break;
      case 'costLink':
        this.state.parametersMentors.costLink = Number(event.target.value);
        break;
      case 'X':
        this.state.parametersMentors.X = Number(event.target.value);
        break;
      case 'Y':
        this.state.parametersMentors.Y = Number(event.target.value);
        break;
      case 'W':
        this.state.parametersMentors.W = Number(event.target.value);
        break;
      case 'R':
        this.state.parametersMentors.R = Number(event.target.value);
        break;
      case 'C':
        this.state.parametersMentors.C = Number(event.target.value);
        break;
      case 'alpha':
        this.state.parametersMentors.alpha = Number(event.target.value);
        break;
      case 'Umin':
        this.state.parametersMentors.Umin = Number(event.target.value);
        break;
      case 'upFile':{
        if(event.target.files[0] != undefined){
          try{
            var arr = [];
            readXlsxFile(event.target.files[0]).then((rows) => {
              rows.forEach((element, index) => {
                if(index >= 2){
                  arr.push(
                    {
                      name: element[0],
                      x: element[1],
                      y: element[2],
                      w: 0,
                      type: ''
                    }
                  )
                }
              });
              this.state.list = arr;
              this.setState({});
            }).catch((err) => {
              alert("File excel không đúng. Vui lòng thử lại");
            })
          }catch(err){
            alert("File excel không đúng. Vui lòng thử lại");
          }          
        }
        break;
      }
      default:
        break;
    }
    this.setState({});
  }

  render() {
    if (!this.state.isOpen) {
      return (
        <Card>
          <CardHeader className='text-left' tag='button' onClick={this.toggle}><strong>Thiết lập ban đầu</strong></CardHeader>
        </Card>
      );
    }

    return (
      <Card>
        <CardHeader><strong>Thiết lập ban đầu</strong></CardHeader>
        <CardBody>
          <Row>
            <Label sm={2}>Nodes:</Label>
            <Col sm={4}>
              <Input
                type='number' id='nodes' name='nodes'
                value={this.state.parametersMentors.nodes}
                onChange={this.changeText}
              />
            </Col>
            <Label sm={2}>cost:</Label>
            <Col sm={4}>
              <Input
                type='number' id='costLink' name='costLink'
                value={this.state.parametersMentors.costLink}
                onChange={this.changeText}
              />
            </Col>
          </Row>
          <br/>
          <Row>
            <Label sm={3}>File tọa độ:</Label>
            <Col sm={9}>
              <Input type="file" name="upFile" id="upFile" onChange={this.changeText}/>
            </Col>
          </Row>
          <br />
          <Row>
            <Label sm={1}>X</Label>
            <Col sm={3}>
              <Input
                type='number' id='X' name='X'
                value={this.state.parametersMentors.X}
                onChange={this.changeText}
              />
            </Col>
            <Label sm={1}>Y</Label>
            <Col sm={3}>
              <Input
                type='number' id='Y' name='Y'
                value={this.state.parametersMentors.Y}
                onChange={this.changeText}
              />
            </Col>
            <Label sm={1}>W</Label>
            <Col sm={3}>
              <Input
                type='number' id='W' name='W'
                value={this.state.parametersMentors.W}
                onChange={this.changeText}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Label sm={1}>R</Label>
            <Col sm={3}>
              <Input
                type='number' id='R' name='R'
                value={this.state.parametersMentors.R}
                onChange={this.changeText}
              />
            </Col>
            <Label sm={1}>C</Label>
            <Col sm={3}>
              <Input
                type='number' id='C' name='C'
                value={this.state.parametersMentors.C}
                onChange={this.changeText}
              />
            </Col>
            <Col sm={1}></Col>
            <Col sm={3}>
              <Input type='checkbox' id='isGrid' /> {'Tạo lưới'}
            </Col>
          </Row>
          <br />
          <Row>
            <Label sm={2}>Alpha</Label>
            <Col sm={4}>
              <Input
                id='alpha' name='alpha'
                value={this.state.parametersMentors.alpha}
                onChange={this.changeText}
              />
            </Col>
            <Label sm={2}>Umin</Label>
            <Col sm={4}>
              <Input
                id='Umin' name='Umin'
                value={this.state.parametersMentors.Umin}
                onChange={this.changeText}
              />
            </Col>
          </Row>
        </CardBody>
        <CardFooter>
          <Row>
            <Col className='text-right'>
              <Button color='primary' onClick={() => {
                if(this.state.list){
                  this.props.setup(this.state.parametersMentors, this.state.list);
                }else{
                  this.props.setupParameters(this.state.parametersMentors);
                }
                this.toggle();
              }}>Cài đặt</Button> {' '}
              <Button color='secondary' onClick={this.toggle}>Thoát</Button>
            </Col>
          </Row>
        </CardFooter>
      </Card>
    );
  }
}

export default SetupBasic;