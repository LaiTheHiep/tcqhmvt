import React from 'react';
import {
  Col, Row, Input, Label, Button, Card, CardBody, CardHeader, CardFooter,
  TabContent, TabPane, Nav, NavItem, NavLink, Container
} from 'reactstrap';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import moment from 'moment';
import classnames from 'classnames';

class ControlSystem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      activeTab: 'a',
      activeButton: 'a1',
    };

    this.toggle = this.toggle.bind(this);
    this.download = this.download.bind(this);
    this.handleMaxCost = this.handleMaxCost.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  download() {
    const { stateTemp } = this.props;
    var arr = [];
    var type = document.getElementById('typeExcel').value;
    switch (type) {
      case 'Danh sách các nút': {
        stateTemp.nodes.forEach(e => {
          arr.push(
            ['', e.name, e.x, e.y, e.w, e.wi, e.type]
          )
        });
        this.downloadDataToExcel('Nodes', [
          ['', '', 'Danh sách thông tin các nút các nút'],
          [],
          ['', 'Tên node', 'Tọa độ X', 'Tọa độ Y', 'Trọng số', 'Trọng số chuẩn hóa', 'Loại nút'],
          ...arr
        ])
        break;
      }
      case 'Lưu lượng giữa các nút': {
        stateTemp.paths.forEach(e => {
          arr.push(
            [e.node1, e.node2, e.dataFlow]
          )
        });
        this.downloadDataToExcel('Paths', [
          ['Danh sách lưu lượng giữa các nút'],
          [],
          ['Node 1', 'Node 2', 'Lưu lượng'],
          ...arr
        ])
        break;
      }
      case 'Thông tin giữa các nút backbone':{
        stateTemp.pathsDataFlowBackBone.forEach((e, i) => {
          if(stateTemp.testCostMap[i]){
            arr.push(
              [e.node1, e.node2, e.dataFlow, stateTemp.testCostMap[i].dataFlow, e.dataFlow / (stateTemp.testCostMap[i].dataFlow * stateTemp.parametersMentors.C)]
            )
          }
        });
        this.downloadDataToExcel('Backbone', [
          ['Thông tin giữa các nút backbone'],
          [],
          ['Node 1', 'Node 2', 'Lưu lượng', 'Số đường sử dụng', 'Độ sử dụng'],
          ...arr
        ])
        break;
      }
      case 'Giá trên liên kết giữa 2 nút backbone':{
        var list = [];
        this.props.stateTemp.backBones.forEach(n1 => {
          this.props.stateTemp.backBones.forEach(n2 => {
            if (n1.name !== n2.name) {
              const { stateTemp } = this.props;
              var tt = stateTemp.backBones.find(e => e.isMomentMin);
              var arr = [];
              if (n1.name == tt.name) {
                var tempN2 = n2;
                arr.push(n2.name);
                do {
                  var temp = stateTemp.backBones.find(e => e.name == tempN2.parent);
                  tempN2 = temp;
                  arr.push(temp.name);
                } while (temp.name != tt.name);
              } else if (n2.name == tt.name) {
                var tempN1 = n1;
                arr.push(n1.name);
                do {
                  var temp = stateTemp.backBones.find(e => e.name == tempN1.parent);
                  tempN1 = temp;
                  arr.push(temp.name);
                } while (temp.name != tt.name);
              } else {
                var tempN2 = n2;
                arr.push(n2.name);
                do {
                  var temp = stateTemp.backBones.find(e => e.name == tempN2.parent);
                  tempN2 = temp;
                  arr.push(temp.name);
                  if (temp.name == n1.name) {
                    break;
                  }
                } while (temp.name != tt.name);
                if (arr[arr.length - 1] == n1.name) {
                  arr = arr;
                } else {
                  var arr1 = [];
                  var tempN1 = n1;
                  arr1.push(n1.name);
                  do {
                    var temp = stateTemp.backBones.find(e => e.name == tempN1.parent);
                    tempN1 = temp;
                    arr1.push(temp.name);
                    if (temp.name == n2.name) {
                      break;
                    }
                  } while (temp.name != tt.name);
                  if (arr1[arr1.length - 1] == n2.name) {
                    arr = arr1;
                  } else {
                    var arr1 = arr1.reverse().filter((e, i) => i != 0);
                    arr = [...arr, ...arr1];
                  }
                }
              }
              var valueCost = this.props.CostLinkBackbone(arr);
              list.push([
                n1.name,
                n2.name,
                valueCost[0],
                valueCost[1]
              ])
            }
          })
        });
        this.downloadDataToExcel('Cost', [
          ['Giá liên kết giữa 2 nút backbone'],
          ['Giá cũ tính theo liên kết trực tiếp của 2 nút backbone (nếu có)'],
          ['Giá mới tính theo cây truy nhập được tạo ra'],
          [],
          ['Node 1', 'Node 2', 'Giá mới', 'Giá cũ'],
          ...list
        ])
      }
      default:
        break;
    }
  }

  downloadDataToExcel(fileName, data) {
    const ws = XLSX.utils.aoa_to_sheet(data);
    const wopts = { bookType: 'xlsx', bookSST: false, type: 'array' }
    const workbook = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(workbook, ws, 'File mẫu')
    const wbout = XLSX.write(workbook, wopts)
    saveAs(
      new Blob([wbout], { type: 'application/octet-stream' }),
      `${fileName}_${moment().format('HH:mm:ss DD-MM-YYYY')}.xlsx`
    )
  }

  handleMaxCost() {
    const { nodes } = this.props.stateTemp;
    var result = this.maxCost(nodes);
    this.props.onMaxCost(result);
  }

  maxCost(nodes) {
    var result = {
      cost: 0
    };
    for (let index = 0; index < nodes.length - 1; index++) {
      for (let i = index + 1; i < nodes.length; i++) {
        let x = nodes[i].x - nodes[index].x;
        let y = nodes[i].y - nodes[index].y;
        let temp = Math.sqrt(x * x + y * y);
        if (temp > result.cost) {
          result = {
            node1: nodes[index].name,
            node2: nodes[i].name,
            cost: temp
          }
        }
      }
    }
    return result;
  }

  render() {
    if (!this.state.isOpen) {
      return (
        <Card>
          <CardHeader className='text-left' tag='button' onClick={this.toggle}><strong>Thanh điều khiển</strong></CardHeader>
        </Card>
      );
    }

    return (
      <Card>
        <CardHeader><strong>Thanh điều khiển</strong></CardHeader>
        <CardBody>
          <Button onClick={() => console.log(this.props.stateTemp)}>Xem thông số</Button>
          <Row>
            <Label sm={4}>Tải danh sách về</Label>
            <Col sm={5}>
              <Input type='select' id='typeExcel' name='typeExcel'>
                <option>Danh sách các nút</option>
                <option>Lưu lượng giữa các nút</option>
                <option>Thông tin giữa các nút backbone</option>
                <option>Giá trên liên kết giữa 2 nút backbone</option>
              </Input>
            </Col>
            <Col sm={3}>
              <Button color='primary' onClick={this.download}>Tải xuống</Button>
            </Col>
          </Row>
          <Row>
            <Container>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === 'a' })}
                    onClick={() => { this.toggleTab('a'); }}
                  >
                    Tạo Backbone
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === 'b' })}
                    onClick={() => { this.toggleTab('b'); }}
                  >
                    Giải thuật Mentor
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeTab === 'c' })}
                    onClick={() => { this.toggleTab('c'); }}
                  >
                    Tính Cost
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId='a'>
                  <Row>
                    <Button
                      color='secondary'
                      onClick={() => {
                        this.setState({
                          activeButton: 'a2'
                        })
                        this.handleMaxCost();
                      }}
                      disabled={this.state.activeButton === 'a1' ? false : true}
                    >
                      Tìm MaxCost
                    </Button> &nbsp;
                    <Button
                      color='secondary'
                      onClick={() => {
                        this.setState({
                          activeButton: 'a3'
                        })
                        this.props.onAccessNode();
                      }}
                      disabled={this.state.activeButton === 'a2' ? false : true}
                    >
                      Tìm nút truy nhập
                    </Button> &nbsp;
                    <Button
                      color='secondary'
                      onClick={() => {
                        this.setState({
                          activeButton: 'b1'
                        })
                        this.props.onMerit();
                      }}
                      disabled={this.state.activeButton === 'a3' ? false : true}
                    >
                      Merit
                    </Button> &nbsp;
                  </Row>
                </TabPane>
                <TabPane tabId='b'>
                  <Button
                    color='secondary'
                    onClick={() => {
                      this.setState({
                        activeButton: 'b2'
                      })
                      this.props.onMoment();
                    }}
                    disabled={this.state.activeButton === 'b1' ? false : true}
                  >
                    Moment
                  </Button> &nbsp;
                  <Button
                    color='secondary'
                    onClick={() => {
                      this.setState({
                        activeButton: 'b3'
                      })
                      this.props.onPrimDijsktra();
                    }}
                    disabled={this.state.activeButton === 'b2' ? false : true}
                  >
                    Prim-Dijsktra
                  </Button> &nbsp;
                  <Button
                    color='secondary'
                    onClick={() => {
                      this.setState({
                        activeButton: 'b4'
                      })
                      this.props.onAddLinkBackbone();
                    }}
                    disabled={this.state.activeButton === 'b3' ? false : true}
                  >
                    Thêm liên kết
                  </Button> &nbsp;
                  <Button
                    color='secondary'
                    onClick={() => {
                      this.setState({
                        activeButton: 'c1'
                      })
                      this.props.onMentor2();
                    }}
                    disabled={this.state.activeButton === 'b4' ? false : true}
                  >
                    Mentor II
                  </Button>
                </TabPane>
                <TabPane tabId='c'>
                  <Row>
                    <Label sm={3}>Node 1</Label>
                    <Col sm={3}>
                      <Input id='node1InputCaculator' />
                    </Col>
                    <Label sm={3}>Node 2</Label>
                    <Col sm={3}>
                      <Input id='node2InputCaculator' />
                    </Col>
                  </Row>
                  <br />
                  <Row>
                    <Label sm={3}>Dãy node:</Label>
                    <Col sm={9}>
                      <Input id='dayInput' />
                    </Col>
                  </Row>
                  <br />
                  <Row>
                    <Container>
                      <Button color='primary' onClick={() => {
                        const { stateTemp } = this.props;
                        var n1 = stateTemp.backBones.find(e => e.name == document.getElementById('node1InputCaculator').value);
                        var n2 = stateTemp.backBones.find(e => e.name == document.getElementById('node2InputCaculator').value);
                        var tt = stateTemp.backBones.find(e => e.isMomentMin);
                        var arr = [];
                        if (n1.name == tt.name) {
                          var tempN2 = n2;
                          arr.push(n2.name);
                          do {
                            var temp = stateTemp.backBones.find(e => e.name == tempN2.parent);
                            tempN2 = temp;
                            arr.push(temp.name);
                          } while (temp.name != tt.name);
                        } else if (n2.name == tt.name) {
                          var tempN1 = n1;
                          arr.push(n1.name);
                          do {
                            var temp = stateTemp.backBones.find(e => e.name == tempN1.parent);
                            tempN1 = temp;
                            arr.push(temp.name);
                          } while (temp.name != tt.name);
                        } else {
                          var tempN2 = n2;
                          arr.push(n2.name);
                          do {
                            var temp = stateTemp.backBones.find(e => e.name == tempN2.parent);
                            tempN2 = temp;
                            arr.push(temp.name);
                            if (temp.name == n1.name) {
                              break;
                            }
                          } while (temp.name != tt.name);
                          if (arr[arr.length - 1] == n1.name) {
                            arr = arr;
                          } else {
                            var arr1 = [];
                            var tempN1 = n1;
                            arr1.push(n1.name);
                            do {
                              var temp = stateTemp.backBones.find(e => e.name == tempN1.parent);
                              tempN1 = temp;
                              arr1.push(temp.name);
                              if (temp.name == n2.name) {
                                break;
                              }
                            } while (temp.name != tt.name);
                            if (arr1[arr1.length - 1] == n2.name) {
                              arr = arr1;
                            } else {
                              var arr1 = arr1.reverse().filter((e, i) => i != 0);
                              arr = [...arr, ...arr1];
                            }
                          }
                        }
                        this.props.CostLinkBackbone(arr);
                      }}>Cost</Button>&nbsp;
                    <Button onClick={() => {
                        var day = document.getElementById('dayInput').value.split(',');
                        var arr = [];
                        day.forEach(e => {
                          arr.push(Number(e))
                        });
                        this.props.CostLinkBackbone(arr);
                      }}>Cost list</Button>
                    </Container>
                  </Row>
                </TabPane>
              </TabContent>
            </Container>
          </Row>
        </CardBody>
        <CardFooter>
          <Row>
            <Col className='text-right'>
              <Button color='secondary' onClick={this.toggle}>Thoát</Button>
            </Col>
          </Row>
        </CardFooter>
      </Card>
    );
  }
}

export default ControlSystem;