import React from 'react';
import { Container, Col, Row, Input, Label, Button, Card, CardBody, CardHeader, CardFooter } from 'reactstrap';
import Table from '../Common/Table';
import readXlsxFile from 'read-excel-file';
import XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import moment from 'moment'

class SetupNodes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [],
      isOpen: false,
      isAutoNode: true
    };

    this.toggle = this.toggle.bind(this);
    this.changeText = this.changeText.bind(this);
    this.addFlow = this.addFlow.bind(this);
    this.downloadTemplate = this.downloadTemplate.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  changeText(event) {
    switch (event.target.name) {
      case 'modeInputNode':{
        if (event.target.value === 'Chế độ nhập nhanh') {
          this.setState({ isAutoNode: true });
        } else {
          this.setState({ isAutoNode: false });
        }
        break;
      }
      case 'upFile':{
        if(event.target.files[0] != undefined){
          try{
            var arr = [];
            readXlsxFile(event.target.files[0]).then((rows) => {
              rows.forEach((element, index) => {
                if(index >= 2){
                  arr.push(
                    {
                      node1: element[0],
                      node2: element[1],
                      dataFlow: element[2],
                    }
                  )
                }
              });
              this.setState({
                list: [
                  ...this.state.list,
                  ...arr
                ]
              })
            }).catch((err) => {
              alert("File excel không đúng. Vui lòng thử lại");
            })
          }catch(err){
            alert("File excel không đúng. Vui lòng thử lại");
          }          
        }
        break;
      }
      default:
        break;
    }
  }

  addFlow() {
    try{
      const {totalNodes} = this.props;
      if(this.state.isAutoNode){
        var n = Number(document.getElementById('n').value);
        var flowData = Number(document.getElementById('flowData').value);
        for (let index = 0; index + n < totalNodes; index++) {
          this.state.list.push(
            {
              node1: index,
              node2: index + n,
              dataFlow: flowData
            }
          )
        }
      } else{
        var n1 = Number(document.getElementById('n1').value);
        var n2 = Number(document.getElementById('n2').value);
        var flowData = Number(document.getElementById('flowData').value);
        this.state.list.push(
          {
            node1: n1,
            node2: n2,
            dataFlow: flowData
          }
        )
      }
      this.setState({});
    } catch(e){
      alert('Sai dữ liệu đầu vào. Hãy thử lại');
    }
  }

  downloadTemplate(){
    var data = [
      ['Danh sách lưu lượng giữa các nút', '', '', '', '', 'Nhập các thông số vào các cột Node 1, Node 2, Lưu lượng'],
      [],
      ['Node 1', 'Node 2', 'Lưu lượng'],
    ]
    const ws = XLSX.utils.aoa_to_sheet(data);
    const wopts = { bookType: 'xlsx', bookSST: false, type: 'array' }
    const workbook = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(workbook, ws, 'File mẫu')
    const wbout = XLSX.write(workbook, wopts)
    saveAs(
      new Blob([wbout], { type: 'application/octet-stream' }),
      `File_mau_${moment().format('HH:mm:ss DD-MM-YYYY')}.xlsx`
    )
  }

  render() {
    const columns = [
      {
        Header: 'STT',
        id: 'stt',
        width: 50,
        Cell: ({ index }) => index + 1,
        filterable: false
      },
      {
        Header: 'Node 1',
        accessor: 'node1',
        filterable: false
      },
      {
        Header: 'Node 2',
        accessor: 'node2',
        filterable: false
      },
      {
        Header: 'Lưu lượng',
        accessor: 'dataFlow',
        filterable: false
      },
      {
        Header: 'Xóa',
        Cell: props => {
          return(
            <Button color='link' onClick={() => {
              this.state.list = this.state.list.filter(e => e !== props.original)
              this.setState({});
            }}><i className='cui-trash'></i></Button>
          );
        },
        filterable: false
      },
    ]
    if (!this.state.isOpen) {
      return (
        <Card>
          <CardHeader className='text-left' tag='button' onClick={this.toggle}><strong>Cài đặt các nút</strong></CardHeader>
        </Card>
      )
    }

    return (
      <Card>
        <CardHeader><strong>Cài đặt các nút</strong></CardHeader>
        <CardBody>
          <Row>
            <Label sm={5}>Lưu lượng giữa các nút:</Label>
            <Col sm={7}>
              <Input type='select' name='modeInputNode' onChange={this.changeText}>
                <option>Chế độ nhập nhanh</option>
                <option>Chế độ thủ công</option>
              </Input>
            </Col>
          </Row>
          <br />
          {
            !this.state.isAutoNode ?
              <div>
                <Row>
                  <Label sm={3}>Nút thứ nhất:</Label>
                  <Col sm={3}>
                    <Input type='number' id='n1' name='n1' />
                  </Col>
                  <Label sm={3}>Nút thứ hai:</Label>
                  <Col sm={3}>
                    <Input type='number' id='n2' name='n2' />
                  </Col>
                </Row>
                <br />
              </div>
              :
              <div>
                <Row>
                  <Label sm={8}>Lưu lượng giữa 2 nút (i, i + n). Nhập giá trị n:</Label>
                  <Col sm={4}>
                    <Input type='number' id='n' name='n' />
                  </Col>
                </Row>
                <br />
              </div>
          }
          <Row>
            <Label sm={4}>Nhập lưu lượng:</Label>
            <Col sm={3}>
              <Input type='number' id='flowData' name='flowData' />
            </Col>
            <Col sm={5}>
              <Button color='danger' onClick={this.addFlow}>Thêm</Button> {'   '}
            </Col>
          </Row>
          <br/>
          <Row>
            <Col sm={7}>
              <Input type="file" name="upFile" id="upFile" onChange={this.changeText}/>
            </Col>
            <Col sm={5}>
              <Button color='success' onClick={this.downloadTemplate}>Tải mẫu</Button>
            </Col>
          </Row>
          <br/>
          <Row className='text-center' style={{ overflow: 'auto' }}>
            <Table
              columns={columns}
              pages={this.state.pages}
              data={this.state.list}
            />
          </Row>
        </CardBody>
        <CardFooter>
          <Row>
            <Col className='text-right'>
              <Button color='primary' onClick={() => {
                this.props.setupNodes(this.state.list);
                this.toggle();
              }}>Cài đặt</Button> {' '}
              <Button color='secondary' onClick={this.toggle}>Thoát</Button>
            </Col>
          </Row>
        </CardFooter>
      </Card>
    )
  }
}

export default SetupNodes;