import React from 'react';
import {
  Container, Col, Row, Modal, ModalBody, ModalHeader, ModalFooter, Input, Label, Button,
  Card, CardBody, CardHeader, CardFooter
} from 'reactstrap';
import JSONInput from 'react-json-editor-ajrm';
import locale from 'react-json-editor-ajrm/locale/en';
import { COLOR } from '../elements/Common/parameter';
import SetupBasic from '../elements/Inputs/SetupBasic';
import SetupNodes from '../elements/Inputs/SetupNodes';
import ControlSystem from '../elements/Inputs/ControlSystem';

class MentorLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paths: [],
      pathsDistance: [],
      pathsDataFlow: [],
      pathsDataFlowBackBone: [],
      addLink: [],
      addLinkMentor2: [],
      nodes: [],
      backBones: [],
      restNodes: [],
      undefinedNodes: [],
      testCostMap:[],
      parametersMentors: {
        nodes: 80,
        X: 1000,
        Y: 1000,
        W: 2,
        R: 0.3,
        C: 10,
        costLink: 0.4,
        alpha: 0.7,
        Umin: 0.7
      },
      isAutoNode: true,
      isToggle: false
    }

    this.changeText = this.changeText.bind(this);
    this.setupParameters = this.setupParameters.bind(this);
    this.setupParametersNode = this.setupParametersNode.bind(this);
    this.setupNodes = this.setupNodes.bind(this);

    this.onMaxCost = this.onMaxCost.bind(this);
    this.onAccessNode = this.onAccessNode.bind(this);
    this.onMerit = this.onMerit.bind(this);
    this.onMoment = this.onMoment.bind(this);
    this.onPrimDijsktra = this.onPrimDijsktra.bind(this);
    this.onAddLinkBackbone = this.onAddLinkBackbone.bind(this);
    this.onMentor2 = this.onMentor2.bind(this);
    this.CostLinkBackbone = this.CostLinkBackbone.bind(this);

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isToggle: !this.state.isToggle
    })
  }

  setupParameters(param) {
    try {
      var conf = window.confirm('Bạn có chắc chắn muốn cài đặt các thông số trên không?');
      if (conf) {
        if (document.getElementById('isGrid').checked) {
          this.setupChart();
        }
        this.paintNode(param.X, param.Y, param.nodes);
        this.setState({
          parametersMentors: param
        })
      }
    } catch (error) {
      alert('Đầu vào không đúng. Xin mời bạn nhập lại');
    }
  }

  setupParametersNode(param, nodes) {
    try {
      var conf = window.confirm('Bạn có chắc chắn muốn cài đặt các thông số trên không?');
      if (conf) {
        if (document.getElementById('isGrid').checked) {
          this.setupChart();
        }
        this.state.nodes = nodes
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        ctx.font = "15px Arial";
        this.state.nodes.forEach((e) => {
          ctx.fillText('.' + e.name, e.x, this.state.parametersMentors.Y - e.y);
        })
        this.setState({
          parametersMentors: param
        })
      }
    } catch (error) {
      alert('Đầu vào không đúng. Xin mời bạn nhập lại');
    }
  }

  setupNodes(param) {
    try {
      var conf = window.confirm('Bạn có chắc chắn muốn cài đặt các thông số  lưu lượng trên không?');
      if (conf) {
        param.forEach((e) => {
          this.state.nodes[e.node1].w += e.dataFlow;
          this.state.nodes[e.node2].w += e.dataFlow;
        });
        this.state.nodes.forEach((e) => {
          var type = this.typeNodes(e.w, this.state.parametersMentors.C, this.state.parametersMentors.W);
          e.wi = type.wi;
          e.type = type.type;
          if (type.type === 'Backbone') {
            this.state.backBones.push(e);
            this.paintBackbone(e);
          } else {
            this.state.restNodes.push(e);
          }
        });
        this.setState({
          paths: param
        })
      }
    } catch (error) {
      alert('Đầu vào không đúng. Xin mời bạn nhập lại');
    }
  }

  changeText(event) {
    switch (event.target.name) {
      case 'modeInputNode': {
        if (event.target.value === 'Nhập nhanh') {
          this.state.isAutoNode = true;
        } else {
          this.state.isAutoNode = false;
        }
        break;
      }
      default: break;
    }

    this.setState({});
  }

  setupChart() {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var i = 0;
    while (i <= this.state.parametersMentors.X) {
      ctx.moveTo(i, 0);
      ctx.lineTo(i, this.state.parametersMentors.Y);
      ctx.stroke();
      i += 100;
    }
    i = 0;
    while (i <= this.state.parametersMentors.Y) {
      ctx.moveTo(0, i);
      ctx.lineTo(this.state.parametersMentors.X, i);
      ctx.stroke();
      i += 100;
    }
  }

  randomNodes(X, Y, nodes) {
    var arr = [];
    for (let index = 0; index < nodes; index++) {
      var x = Math.floor(Math.random() * X);
      var y = Math.floor(Math.random() * Y);
      var check = arr.find(e => e.x === x && e.y === y);
      if (check === undefined) {
        arr.push(
          {
            name: index,
            x: x,
            y: y,
            w: 0,
            type: ''
          }
        )
      }
    }
    return arr;
  }

  convertPositionView() {
    this.state.nodes.forEach((e) => {
      e.y = this.state.parametersMentors.Y - e.y;
    })
  }

  typeNodes(Wi, C, W) {
    return Wi / C > W ? { wi: Wi / C, type: 'Backbone' } : { wi: Wi / C, type: '' };
  }

  paintNode(X, Y, nodes) {
    this.state.nodes = this.randomNodes(X, Y, nodes);
    this.convertPositionView();
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.font = "15px Arial";
    this.state.nodes.forEach((e) => {
      ctx.fillText('.' + e.name, e.x, this.state.parametersMentors.Y - e.y);
    })
  }

  paintBackbone(node) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.rect(node.x - 5, this.state.parametersMentors.Y - node.y - 5, 10, 10);
    ctx.stroke();
  }

  paintCircle(x, y, R) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.arc(x, y, R, 0, 2 * Math.PI);
    ctx.stroke();
  }

  paintLine(node1, node2) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(node1.x, this.state.parametersMentors.Y - node1.y);
    ctx.lineTo(node2.x, this.state.parametersMentors.Y - node2.y);
    ctx.strokeStyle = 'black';
    ctx.stroke();
  }

  setColor() {
    this.state.backBones.forEach((e, i) => {
      e.color = COLOR[i % COLOR.length]
    });
  }

  onMaxCost(param) {
    this.setState({
      maxCost: param,
      MAXCOST: param.cost * this.state.parametersMentors.R
    });
  }

  getDistance(n1, n2) {
    var x = n1.x - n2.x;
    var y = n1.y - n2.y;

    return Math.sqrt(x * x + y * y);
  }

  onAccessNode() {
    const { backBones, restNodes, MAXCOST } = this.state;
    for (let index = 0; index < restNodes.length; index++) {
      var temp = {};
      for (let i = 0; i < backBones.length; i++) {
        var disc = this.getDistance(backBones[i], restNodes[index]);
        if (MAXCOST >= disc) {
          if (temp.node === undefined || temp.disc >= disc) {
            temp = {
              node: backBones[i],
              disc: disc
            }
          }
        }
      }
      if (temp.node !== undefined) {
        restNodes[index].type = temp.node.name;
        this.paintLine(restNodes[index], temp.node);
        this.paintCircle(restNodes[index].x, this.state.parametersMentors.Y - restNodes[index].y, 3);
      }
    }
    this.state.undefinedNodes = this.state.restNodes.filter((e, i) => e.type === '');
    this.state.restNodes = this.state.restNodes.filter((e, i) => e.type !== '');
    this.setState({});
  }

  onMerit() {
    while (this.state.undefinedNodes.length) {
      var wn = 0; // Tong trong so
      var x = 0, y = 0; // Toa do trung tam
      var maxdc = 0;
      var maxW = 0;
      var merit = 0;
      var indexMerit = 0;
      if (this.state.undefinedNodes.length === 1) {
        this.state.undefinedNodes[0].type = 'Backbone';
        this.state.backBones.push(this.state.undefinedNodes[0]);
        this.paintBackbone(this.state.undefinedNodes[0]);
        this.state.undefinedNodes = [];
        this.setState({});
        break;
      } else {
        this.state.undefinedNodes.forEach((e, i) => {
          wn += e.w;
          x += e.x * e.w;
          y += e.y * e.w;
        })
        x = x / wn;
        y = y / wn;
        this.state.undefinedNodes.forEach((e, i) => {
          e.dc = Math.sqrt((e.x - x) * (e.x - x) + (e.y - y) * (e.y - y));
          if (maxdc < e.dc) {
            maxdc = e.dc
          }
          if (maxW < e.w) {
            maxW = e.w
          }
        })
        this.state.undefinedNodes.forEach((e, i) => {
          let meritTemp = 0.5 * (maxdc - e.dc) / maxdc + 0.5 * e.w / maxW;
          if (merit < meritTemp) {
            merit = meritTemp;
            indexMerit = i;
          }
        })
        this.state.undefinedNodes[indexMerit].type = 'Backbone';
        var newBackBone = this.state.undefinedNodes[indexMerit];
        this.state.backBones.push(newBackBone);
        this.paintBackbone(newBackBone);
        this.state.undefinedNodes.splice(indexMerit, 1);
        this.state.undefinedNodes.forEach((e, i) => {
          var disc = this.getDistance(newBackBone, e);
          if (this.state.MAXCOST >= disc) {
            e.type = newBackBone.name;
            this.paintLine(newBackBone, e);
            this.paintCircle(e.x, this.state.parametersMentors.Y - e.y, 3);
            this.state.restNodes.push(e);
          }
        })
        this.state.undefinedNodes = this.state.undefinedNodes.filter((e, i) => e.type === '');
        this.setState({});
      }
    }
  }

  onMoment() {
    this.state.backBones.forEach((e, i) => {
      e.moment = 0;
      this.state.backBones.forEach((rest) => {
        if (rest.name !== e.name) {
          e.moment += this.getDistance(e, rest) * e.w;
        }
      });
    });
    var momentMin = 0;
    var indexBackbone = null;
    this.state.backBones.forEach((e, i) => {
      if (momentMin < e.moment) {
        indexBackbone = i;
        momentMin = e.moment;
      }
    });
    if (indexBackbone !== null) {
      this.state.backBones[indexBackbone].isMomentMin = true;
    }
    this.setState({});
  }

  setupDistanceBackbone() {
    for (let i = 0; i < this.state.backBones.length - 1; i++) {
      for (let j = i + 1; j < this.state.backBones.length; j++) {
        this.state.pathsDistance.push({
          node1: this.state.backBones[i].name,
          node2: this.state.backBones[j].name,
          link: this.getDistance(this.state.backBones[i], this.state.backBones[j]) * this.state.parametersMentors.costLink,
        })

      }
    }
  }

  onPrimDijsktra() {
    this.setupDistanceBackbone();
    var U = [];
    var V = [];
    this.state.backBones.forEach(e => {
      if (e.isMomentMin) {
        e.VertexSource = 0;
        U.push(e);
      } else {
        V.push(e);
        e.VertexSource = 10000000;
      }
    });
    while (U.length < this.state.backBones.length) {
      //get all path
      var arr = [];
      U.forEach(e => {
        this.state.pathsDistance.forEach(p => {
          if (p.node1 == e.name || p.node2 == e.name) {
            if (!(U.find(u => u.name == p.node1) && U.find(u => u.name == p.node2))) {
              arr.push(p);
            }
          }
        });
      })

      // find min dist
      var result = {
        link: 10000000,
      };
      arr.forEach(e => {
        let d1 = e.link;
        if (U.find(u => u.name == e.node1)) {
          let d2 = U.find(u => u.name == e.node1).VertexSource * this.state.parametersMentors.alpha;
          if (result.link > d1 + d2) {
            result = e;
            result.link = d1 + d2;
          }
        } else {
          let d2 = U.find(u => u.name == e.node2).VertexSource * this.state.parametersMentors.alpha;
          if (result.link > d1 + d2) {
            result = e;
            result.link = d1 + d2;
          }
        }
      });
      if (U.find(u => u.name == result.node1)) {
        this.state.backBones.find(b => b.name == result.node2).parent = result.node1;
        this.state.backBones.find(b => b.name == result.node2).VertexSource = result.link;
        U.push(this.state.backBones.find(b => b.name == result.node2));
        V = V.filter(v => v.name != this.state.backBones.find(b => b.name == result.node2).name);
      } else {
        this.state.backBones.find(b => b.name == result.node1).parent = result.node2;
        this.state.backBones.find(b => b.name == result.node1).VertexSource = result.link;
        U.push(this.state.backBones.find(b => b.name == result.node1));
        V = V.filter(v => v.name != this.state.backBones.find(b => b.name == result.node1).name);
      }
    }

    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    this.state.backBones.forEach((e) => {
      if (!e.isMomentMin) {
        let node1 = this.state.backBones.find(n => n.name === e.name);
        let node2 = this.state.backBones.find(n => n.name === e.parent);
        if (node1 && node2) {
          ctx.moveTo(node1.x, this.state.parametersMentors.Y - node1.y);
          ctx.lineTo(node2.x, this.state.parametersMentors.Y - node2.y);
        }
      }
    })
    ctx.strokeStyle = 'red';
    ctx.stroke();
    this.setState({});
    this.paintBackbone(this.state.backBones.find(e => e.isMomentMin));
    ctx.strokeStyle = 'red';
    ctx.stroke();
  }

  onAddLinkBackbone() {
    this.state.paths.forEach((e) => {
      let node1 = this.state.backBones.find(n => n.name == e.node1);
      let node2 = this.state.backBones.find(n => n.name == e.node2);
      if (node1 && node2) {
        this.state.pathsDataFlow.push(e);
      }
    });
    this.state.pathsDataFlow.forEach((e, i) => {
      var n = Math.ceil(e.dataFlow / this.state.parametersMentors.C);
      var u = e.dataFlow / (n * this.state.parametersMentors.C);
      if (u < this.state.parametersMentors.Umin) {
        var result = {
          cost: 1000000
        }
        let node1 = this.state.backBones.find(n => n.name == e.node1);
        let node2 = this.state.backBones.find(n => n.name == e.node2);
        this.state.backBones.forEach(node => {
          if (node.name != e.node1 && node.name != e.node2) {
            if (node1 && node2) {
              let tempCost = (this.getDistance(node, node1) + this.getDistance(node, node2)) * this.state.parametersMentors.costLink;
              if (tempCost < result.cost) {
                result = {
                  cost: tempCost,
                  home: node.name
                }
              }
            }
          }
        });
        let home = this.state.backBones.find(n => n.name == result.home);
        if (home) {
          var Tb1 = this.state.pathsDataFlow.find(p => (p.node1 == node1.name && p.node2 == home.name) || (p.node2 == node1.name && p.node1 == home.name));
          if (Tb1) {
            Tb1.dataFlow += e.dataFlow;
          } else {
            this.state.pathsDataFlow.push({
              node1: node1.name,
              node2: home.name,
              dataFlow: e.dataFlow
            })
          }
          var Tb2 = this.state.pathsDataFlow.find(p => (p.node1 == node2.name && p.node2 == home.name) || (p.node2 == node2.name && p.node1 == home.name));
          if (Tb2) {
            Tb2.dataFlow += e.dataFlow;
          } else {
            this.state.pathsDataFlow.push({
              node1: node2.name,
              node2: home.name,
              dataFlow: e.dataFlow
            })
          }
        }
      } else {
        this.state.addLink.push({
          node1: e.node1,
          node2: e.node2,
          dataFlow: n
        });
      }
    });
    //paint
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    this.state.addLink.forEach((e) => {
      let node1 = this.state.backBones.find(n => n.name === e.node1);
      let node2 = this.state.backBones.find(n => n.name === e.node2);
      if (node1 && node2) {
        ctx.moveTo(node1.x, this.state.parametersMentors.Y - node1.y);
        ctx.lineTo(node2.x, this.state.parametersMentors.Y - node2.y);
      }
    })
    ctx.strokeStyle = 'blue';
    ctx.stroke();
  }

  onMentor2() {
    this.onDataBackbone();
    this.onSetBackbone();
    var array = [...this.state.pathsDataFlow, ...this.state.addLink];
    // setup matrix
    this.state.backBones.forEach((e) => {
      e.mentor2 = {};
      this.state.backBones.forEach((elm) => {
        if (e.name != elm.name) {
          let temp = array.find(a => (a.node1 == e.name && a.node2 == elm.name) || (a.node2 == e.name && a.node1 == elm.name));
          if (temp) {
            e.mentor2[elm.name] = temp.dataFlow;
          } else {
            e.mentor2[elm.name] = 1000000;
          }
        } else {
          e.mentor2[e.name] = 0;
        }
      })
    });

    this.state.backBones.forEach(e => {
      this.state.backBones.forEach(element => {
        Object.keys(element.mentor2).forEach(key => {
          if (element.mentor2[key] > element.mentor2[e.name] + e.mentor2[key]) {
            element.mentor2[key] = element.mentor2[e.name] + e.mentor2[key];
          }
        })
      })
    });
    // this.setState({});
    this.testCost();
  }

  onDataBackbone() {
    this.state.pathsDataFlowBackBone = [];
    for (let i = 0; i < this.state.backBones.length - 1; i++) {
      for (let j = i + 1; j < this.state.backBones.length; j++) {
        //
        var l1 = this.state.nodes.filter(e => e.type == this.state.backBones[i].name);
        var l2 = this.state.nodes.filter(e => e.type == this.state.backBones[j].name);
        var result = 0;
        l1.forEach(e1 => {
          l2.forEach(e2 => {
            let path = this.state.paths.find(e => (e.node1 == e1.name && e.node2 == e2.name) || (e.node2 == e1.name && e.node1 == e2.name));
            if (path) {
              result += path.dataFlow;
            }
          })
        })
        var pathBB = this.state.paths.find(e => (e.node1 == this.state.backBones[i].name && e.node2 == this.state.backBones[j].name) || (e.node2 == this.state.backBones[j].name && e.node1 == this.state.backBones[i].name));
        if (pathBB) {
          result += pathBB.dataFlow;
        }
        var pathAdd = this.state.addLink.find(e => (e.node1 == this.state.backBones[i].name && e.node2 == this.state.backBones[j].name) || (e.node2 == this.state.backBones[j].name && e.node1 == this.state.backBones[i].name));
        if(pathAdd){
          result += pathAdd.dataFlow;
        }
        this.state.pathsDataFlowBackBone.push({
          node1: this.state.backBones[i].name,
          node2: this.state.backBones[j].name,
          dataFlow: result
        })
        //
      }
    }
    this.setState({});
  }

  onSetBackbone(){
    this.state.pathsDataFlowBackBone.forEach(path => {
      var n1 = this.state.backBones.find(e => e.name == path.node1);
      var n2 = this.state.backBones.find(e => e.name == path.node2);
      if(n1 && n2 && path.dataFlow != 0 && n1.parent != n2.name && n2.parent != n1.name){
        var L = this.getDistance(n1, n2) * this.state.parametersMentors.costLink;
        var s_list = [];
        var d_list = [];
        var array = [];
        this.state.backBones.forEach(e => {
          if(e.name != n1.name && e.name != n2.name){
            if(this.getDistance(e, n1) * this.state.parametersMentors.cost + L < this.getDistance(e, n2) * this.state.parametersMentors.cost){
              s_list.push(e)
            } else if(this.getDistance(e, n2) * this.state.parametersMentors.cost + L < this.getDistance(e, n1) * this.state.parametersMentors.cost){
              d_list.push(e);
            } else{}
          }
        });
        s_list.push(n1);
        d_list.push(n2);
        array.push(L);
        for (let i = 0; i < s_list.length; i++) {
          for (let j = 0; j < d_list.length; j++) {
            let maxL = this.getDistance(s_list[i], d_list[j]) - this.getDistance(s_list[i], n1) - this.getDistance(d_list[j], n2);
            maxL = maxL * this.state.parametersMentors.costLink;
            if(maxL > L){
              array.push(maxL);
            }
          }
        }
        if(array.length > 1){
          var max1 = Math.max(...array);
          var max2 = Math.max(...array.filter(e => e < Math.max(...array)));
          var result = Math.ceil(max2);
          if(result < max1){
            this.state.addLinkMentor2.push({
              node1: n1.name,
              node2: n2.name,
              dataFlow: result
            })
          }
        }

      }
    });
    this.setState({});
  }

  CostLinkBackbone(arr){
    var oldCost = 0;
    var newCost = 0;
    for (let i = 0; i < arr.length - 1; i++) {
      var n1 = this.state.backBones.find(e => e.name == arr[i]);
      var n2 = this.state.backBones.find(e => e.name == arr[i + 1]);
      oldCost += this.getDistance(n1, n2) * this.state.parametersMentors.costLink;
    }
    var x = this.state.pathsDataFlowBackBone.find(e => (e.node1 == arr[0] && e.node2 == arr[arr.length - 1]) || (e.node2 == arr[0] && e.node1 == arr[arr.length - 1]));
    if(x){
      newCost = this.getDistance(this.state.backBones.find(e => e.name == arr[0]), this.state.backBones.find(e => e.name == arr[arr.length - 1])) * this.state.parametersMentors.costLink;
    }
    return [oldCost, newCost];
    // console.log('newCost: ', oldCost);
    // console.log('oldCost: ', newCost);
    // alert(`newCost: ${oldCost}\noldCost: ${newCost}`);

  }

  testCost(){
    var arr = [];
    var result = [];
    this.state.pathsDataFlowBackBone.sort(function(a, b){return b.dataFlow - a.dataFlow});
    this.state.pathsDataFlowBackBone = this.state.pathsDataFlowBackBone.filter((e) => e.dataFlow != 0);
    this.state.pathsDataFlowBackBone.map(e => {
      var n = Math.ceil(e.dataFlow / this.state.parametersMentors.C);
      var u = e.dataFlow / (n * this.state.parametersMentors.C);
      // if (u < this.state.parametersMentors.Umin) {
        var result = {
          cost: 1000000
        }
        let node1 = this.state.backBones.find(n => n.name == e.node1);
        let node2 = this.state.backBones.find(n => n.name == e.node2);
        this.state.backBones.forEach(node => {
          if (node.name != e.node1 && node.name != e.node2) {
            if (node1 && node2) {
              let tempCost = (this.getDistance(node, node1) + this.getDistance(node, node2)) * this.state.parametersMentors.costLink;
              if (tempCost < result.cost) {
                result = {
                  cost: tempCost,
                  home: node.name
                }
              }
            }
          }
        });
        let home = this.state.backBones.find(n => n.name == result.home);
        if (home) {
          var Tb1 = this.state.pathsDataFlow.find(p => (p.node1 == node1.name && p.node2 == home.name) || (p.node2 == node1.name && p.node1 == home.name));
          if (Tb1) {
            Tb1.dataFlow += e.dataFlow;
          } else {
            this.state.pathsDataFlow.push({
              node1: node1.name,
              node2: home.name,
              dataFlow: e.dataFlow
            })
          }
          var Tb2 = this.state.pathsDataFlow.find(p => (p.node1 == node2.name && p.node2 == home.name) || (p.node2 == node2.name && p.node1 == home.name));
          if (Tb2) {
            Tb2.dataFlow += e.dataFlow;
          } else {
            this.state.pathsDataFlow.push({
              node1: node2.name,
              node2: home.name,
              dataFlow: e.dataFlow
            })
          }
        }
      // } else {
        this.state.testCostMap.push({
          node1: e.node1,
          node2: e.node2,
          dataFlow: n
        });
      // }

    });
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    this.state.testCostMap.forEach((e) => {
      let node1 = this.state.backBones.find(n => n.name === e.node1);
      let node2 = this.state.backBones.find(n => n.name === e.node2);
      if (node1 && node2) {
        ctx.moveTo(node1.x, this.state.parametersMentors.Y - node1.y);
        ctx.lineTo(node2.x, this.state.parametersMentors.Y - node2.y);
      }
    })
    ctx.strokeStyle = 'blue';
    ctx.stroke();
    this.setState({});
  }

  render() {
    return (
      <Container>
        {/* <Button
          onClick={() => {
            var arr = [];
            var result = [];
            this.state.pathsDataFlowBackBone.sort(function(a, b){return b.dataFlow - a.dataFlow});
            this.state.pathsDataFlowBackBone = this.state.pathsDataFlowBackBone.filter((e) => e.dataFlow != 0);
            this.state.pathsDataFlowBackBone.map(e => {
              var n = Math.ceil(e.dataFlow / this.state.parametersMentors.C);
              var u = e.dataFlow / (n * this.state.parametersMentors.C);
              // if (u < this.state.parametersMentors.Umin) {
                var result = {
                  cost: 1000000
                }
                let node1 = this.state.backBones.find(n => n.name == e.node1);
                let node2 = this.state.backBones.find(n => n.name == e.node2);
                this.state.backBones.forEach(node => {
                  if (node.name != e.node1 && node.name != e.node2) {
                    if (node1 && node2) {
                      let tempCost = (this.getDistance(node, node1) + this.getDistance(node, node2)) * this.state.parametersMentors.costLink;
                      if (tempCost < result.cost) {
                        result = {
                          cost: tempCost,
                          home: node.name
                        }
                      }
                    }
                  }
                });
                let home = this.state.backBones.find(n => n.name == result.home);
                if (home) {
                  var Tb1 = this.state.pathsDataFlow.find(p => (p.node1 == node1.name && p.node2 == home.name) || (p.node2 == node1.name && p.node1 == home.name));
                  if (Tb1) {
                    Tb1.dataFlow += e.dataFlow;
                  } else {
                    this.state.pathsDataFlow.push({
                      node1: node1.name,
                      node2: home.name,
                      dataFlow: e.dataFlow
                    })
                  }
                  var Tb2 = this.state.pathsDataFlow.find(p => (p.node1 == node2.name && p.node2 == home.name) || (p.node2 == node2.name && p.node1 == home.name));
                  if (Tb2) {
                    Tb2.dataFlow += e.dataFlow;
                  } else {
                    this.state.pathsDataFlow.push({
                      node1: node2.name,
                      node2: home.name,
                      dataFlow: e.dataFlow
                    })
                  }
                }
              // } else {
                this.state.testCostMap.push({
                  node1: e.node1,
                  node2: e.node2,
                  dataFlow: n
                });
              // }
        
            });
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext("2d");
            ctx.beginPath();
            this.state.testCostMap.forEach((e) => {
              let node1 = this.state.backBones.find(n => n.name === e.node1);
              let node2 = this.state.backBones.find(n => n.name === e.node2);
              if (node1 && node2) {
                ctx.moveTo(node1.x, this.state.parametersMentors.Y - node1.y);
                ctx.lineTo(node2.x, this.state.parametersMentors.Y - node2.y);
              }
            })
            ctx.strokeStyle = 'blue';
            ctx.stroke();
            this.setState({});
          }}
        >Test</Button> */}
        <Row>
          <Col sm={6}>
            <SetupBasic
              parametersMentors={this.state.parametersMentors}
              setupParameters={this.setupParameters}
              setup={this.setupParametersNode}
            />
          </Col>
          <Col sm={6}>
            <SetupNodes
              totalNodes={this.state.parametersMentors.nodes}
              setupNodes={this.setupNodes}
            />
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            <ControlSystem
              stateTemp={this.state}
              onMaxCost={this.onMaxCost}
              onAccessNode={this.onAccessNode}
              onMerit={this.onMerit}
              onMoment={this.onMoment}
              onPrimDijsktra={this.onPrimDijsktra}
              onAddLinkBackbone={this.onAddLinkBackbone}
              onMentor2={this.onMentor2}
              CostLinkBackbone={this.CostLinkBackbone}
            />
          </Col>
          <Col sm={6}>
            {
              !this.state.isToggle ?
                <Card>
                  <CardHeader className='text-left' tag='button' onClick={this.toggle}>
                    <strong>Tất cả thông số</strong>
                  </CardHeader>
                </Card>
                :
                <Card>
                  <CardHeader><strong>Tất cả thông số</strong></CardHeader>
                  <CardBody>
                    <JSONInput
                      id='a_unique_id'
                      placeholder={this.state}
                      locale={locale}
                      height='550px'
                      width='450px'
                    />
                  </CardBody>
                  <CardHeader>
                    <div className='text-right'>
                      <Button color='secondary' onClick={this.toggle}>Thoát</Button>
                    </div>
                  </CardHeader>
                </Card>
            }
          </Col>
        </Row>
        <hr />
        <Row>
          <Container style={{ overflow: 'auto' }}>
            <canvas
              id='myCanvas'
              width={this.state.parametersMentors.X}
              height={this.state.parametersMentors.Y}
              style={{ border: '1px solid #000000' }}
            >
            </canvas>
          </Container>
        </Row>
      </Container>
    );
  }
}

export default MentorLayout;