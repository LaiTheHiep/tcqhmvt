import React from 'react';
import { Container, Col, Row, Label, Input, Button, Card, CardBody, CardHeader, CardFooter } from 'reactstrap';
import Img from 'react-image';
// import gitPrj from '../../../../resources/imgs/gitPrj.png';
import gitPrj from './imgs/gitPrj.png';
import dashboard from './imgs/dashboard.png';
class SettingLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isNode: false,
      isGit: false,
      isProject: false,
    }

    this.toggleNode = this.toggleNode.bind(this);
    this.toggleGit = this.toggleGit.bind(this);
    this.toggleProject = this.toggleProject.bind(this);
  }

  toggleNode() {
    this.setState({
      isNode: !this.state.isNode
    });
  }

  toggleGit() {
    this.setState({
      isGit: !this.state.isGit
    });
  }

  toggleProject() {
    this.setState({
      isProject: !this.state.isProject
    });
  }

  render() {
    return (
      <div>
        <h1>Hướng dẫn cài đặt project</h1>
        <hr />
        <Button
          color='link'
          style={{ color: 'black' }}
          onClick={() => { window.open('https://nodejs.org/en/download/') }}
        >
          <h3>1. Cài đặt Nodejs</h3>
        </Button>
        <Button color='link' onClick={this.toggleNode}><i className='icon-plus'></i></Button>
        {
          this.state.isNode &&
          <Card>
            <CardHeader><strong>Hướng dẫn cài đặt Nodejs</strong></CardHeader>
            <CardBody>
              <div>
                <strong>NodeJS</strong> là một môi trường chạy
                <strong>JavaScript</strong> (
                <strong>JavaScript Runtime Environment</strong>) bên ngoài trình duyệt.
                <strong>NodeJS</strong> cũng bao gồm các thành phần, thư viện khác để nó có thể hoạt động như một 
                <strong> Web Application Server</strong>.
              </div>
              <div>
                Cài đặt Nodejs các bạn lên trang chủ để cài đặt (hỗ trợ cả Linux, Window, MaxOS).
                <p style={{color: '#40abd8', cursor:'pointer'}} onClick={() => window.open('https://nodejs.org/en/download/')}>Trang chủ nodejs</p>
              </div>
            </CardBody>
            <CardFooter>
              <div className='text-right'><Button onClick={this.toggleNode}>Thoát</Button></div>
            </CardFooter>
          </Card>
        }
        <br />
        <Button
          color='link'
          style={{ color: 'black' }}
          onClick={() => { window.open('https://desktop.github.com/') }}
        >
          <h3>2. Cài đặt Git</h3>
        </Button>
        <Button color='link' onClick={this.toggleGit}><i className='icon-plus'></i></Button>
        {
          this.state.isGit &&
          <Card>
            <CardHeader><strong>Hướng dẫn cài đặt Git</strong></CardHeader>
            <CardBody>
            <div>
	            <strong>Git</strong> là một hệ thống quản lý phiên bản phân tán 
              (Distributed Version Control System – DVCS) ra đời vào năm 2005 và hiện được dùng rất phổ biến. 
              So với các hệ thống quản lý phiên bản tập trung khi tất cả mã nguồn và lịch sử thay đổi chỉ được 
              lưu một nơi là máy chủ thì trong hệ thống phân tán, các máy khách không chỉ "check out" 
              phiên bản mới nhất của các tập tin mà là sao chép (mirror) toàn bộ kho mã nguồn (repository). 
              Như vậy, nếu như máy chủ ngừng hoạt động, thì bạn hoàn toàn có thể lấy kho chứa từ bất kỳ máy khách nào 
              để sao chép ngược trở lại máy chủ để khôi phục lại toàn bộ hệ thống. 
              Mỗi checkout thực sự là một bản sao đầy đủ của tất cả dữ liệu của kho chứa từ máy chủ.&nbsp;
            </div>
            <div>
              Hướng dẫn sử dụng và cách cài đặt bạn lên trang chủ để được hướng dẫn chi tiết
              <p style={{color: '#40abd8', cursor:'pointer'}} onClick={() => window.open('https://git-scm.com/')}>Trang chủ Git</p>
            </div>
            </CardBody>
            <CardFooter>
              <div className='text-right'><Button onClick={this.toggleGit}>Thoát</Button></div>
            </CardFooter>
          </Card>
        }
        <br />
        <Button
          color='link'
          style={{ color: 'black' }}
          onClick={() => { window.open('https://gitlab.com/LaiTheHiep/tcqhmvt') }}
        >
          <h3>3. Clone project</h3>
        </Button>
        <Button color='link' onClick={this.toggleProject}><i className='icon-plus'></i></Button>
        {
          this.state.isProject &&
          <Card>
            <CardHeader><strong>Hướng dẫn cài đặt project</strong></CardHeader>
            <CardBody>
              <strong>Bước 1: </strong>Các bạn truy cập đường link sau để dowload source code:
              <Button color='link' onClick={() => window.open('https://gitlab.com/LaiTheHiep/tcqhmvt')}>https://gitlab.com/LaiTheHiep/tcqhmvt</Button>
              <br/><strong>Bước 2: </strong>Tải source code về máy. Sử dụng chức năng clone hoặc download
              <br/><img src={gitPrj} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} />
              <br/><strong>Bước 3: </strong>Sử dụng Terminal của Linux, MaxOS hoặc cmd của Window. Sau đó dùng câu lệnh cd để trỏ tới thư mục source code
              <br/><strong>Bước 4: </strong>Gõ yarn install hoặc npm install (tùy thuộc vào việc bạn cài yarn hay npm lúc cài nodejs) để  tải module thư viện xuống
              <br/><strong>Bước 5: </strong>Gõ yarn start hoặc npm start để chạy project
              <br/><img src={dashboard} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} />
              <br/> Hoặc bạn có thể lên website theo đường link dưới đây:
              <Button color='link' onClick={() => {window.open('https://quanlycafe.000webhostapp.com/index.html')}}>https://quanlycafe.000webhostapp.com/index.html</Button>
            </CardBody>
            <CardFooter>
              <div className='text-right'><Button onClick={this.toggleProject}>Thoát</Button></div>
            </CardFooter>
          </Card>
        }
        <br />
      </div>
    );
  }
}

export default SettingLayout;