import React from 'react';
import { Container, Col, Row, Label, Input } from 'reactstrap';
import p1 from './imgs/p1.png';
import p2 from './imgs/p2.png';
import p3a from './imgs/p3a.png';
import p3b from './imgs/p3b.png';
import p4 from './imgs/p4.png';
import p5 from './imgs/p5.png';
import p6 from './imgs/p6.png';
import p7 from './imgs/p7.png';
import p8 from './imgs/p8.png';

class TutorialLayout extends React.Component{
  render(){
    return(
      <div>
        <h1>Hướng dẫn sử dụng trang web</h1>
        <hr />
        <h3>1. Giao diện trang project gồm 4 option:</h3>
        <p> + <b>Thiết lập ban đầu: </b>Cài đặt các tham số mà đề bài yêu cầu</p>
        <p> + <b>cài đặt các nút: </b>Cài đặt lưu lượng giữa các nút</p>
        <p> + <b>Thanh điều khiển: </b>Tính toán đưa ra kết quả</p>
        <p> + <b>Tất cả thông số: </b>Xem tất cả thông số dưới dạng json</p>
        <br/><img src={p1} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <h3>2. Thiết lập ban đầu: </h3>
        <p> + Nhập các thông số để hệ thống tự random tọa độ các nút</p>
        <p> + Ngoài ra ta cũng có thể thiết lập tọa độ các nút dưới dạng file excel với 2 cột Node1, X, Y</p>
        <br/><img src={p2} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <h3>3. Cài đặt các nút: </h3>
        <p> + Ta có thế nhập thủ công từng nút</p>
        <br/><img src={p3b} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <p> + Ta có thế nhập các yêu cầu nhanh theo giả thiết</p>
        <br/><img src={p3a} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <p> + Ngoài ra ta có thế  tạo file lưu lượng rồi upload lên. Xem file mẫu bằng cách nhấn nút <b>Tải mẫu</b> để biết cách tải</p>
        <h3>4. Thanh điều khiển</h3>
        <p> + Sau khi đã làm xong lần lượt phần 1 và phần 2. Ta tiến hành click vào các nút theo phương pháp làm giải thuật. 
            Kết quả chính là sơ đồ bên dưới
        </p>
        <br/><img src={p4} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <p> + Ta có thế download các file khi đã nhấn chạy toàn bộ các chức năng trong 2 phần 1 và 2</p>
        <p><b>Danh sách các nút: </b>ta có thể xem trọng số, tọa độ và chức năng của từng nút (backbone hay nút truy nhập)</p>
        <p><b>Lưu lượng giữa các nút: </b>ta có thể xem lưu lượng của toàn bộ cấu hình giả thiết ta đã cài đặt</p>
        <p><b>Thông tin giữa các nút backbone: </b>ta có thể xem lưu lượng giữa các nút backbone, số đường sử dụng và độ sử dụng trên từng liên kết đó</p>
        <p><b>Giá trên liên kết giữa 2 nút backbone: </b>ta có thể xem tất cả giá giữa 2 nút backbone bất kì với nhau. Giá cũ chính là liên kết trực tiếp (có hoặc không), giá mới là giá đã được xử lý thông qua thuật toán</p>
        <p> + Tiếp theo là ta tính theo giải thuật mentor đề xét lưu lượng, chi phí...</p>
        <br/><img src={p5} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <p> + So sánh cost theo cây Prim-Disktra và cost khi đã thêm liên kết trực tiếp bằng cách nhập 2 nút vào ô node1 và node2</p>
        <p>Ngoài ra ta có thể kiểm tra bằng cách nhập dãy nút backbone(n1,n2,n3...) để tính cost mà chúng ta quan sát trên sơ đồ (cost từ node đầu tới node cuối)</p>
        <br/><img src={p6} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <h3>5. Tất cả các thông số</h3>
        <p> + Ở đây ta có thế xem các thông số trong suốt quá trình ta thực hiện phép tính</p>
        <br/><img src={p7} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
        <p> + Để xem thông số dễ dàng hơn ta cũng có thế xem bằng cách nhấn nút <b>Xem thông số</b> trong <b>Thanh điều khiển</b>. Sau đó bật f12 (Ctrl + Shift + I) hoặc chuột phải vào màn hình chọn Inspect và chọn chế độ Console</p>
        <br/><img src={p8} style={{height:500, width:'100%', objectFit:'contain', cursor:'pointer'}} /> <br/>
      </div>
    );
  }
}

export default TutorialLayout;