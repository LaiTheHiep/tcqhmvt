import React from 'react';
import Table from 'react-table';
import 'react-table/react-table.css'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      members: [
        {
          name: 'Lại Thế Hiệp',
          class: 'Điện tử 01 - K60',
          position: 'Trưởng nhóm',
          MSSV: '20151424'
        },
        {
          name: 'Đỗ  Thị Thúy Kiều',
          class: 'Điện tử 08 - K60',
          position: 'Thành viên',
          MSSV: '20152085'
        },
        {
          name: 'Phan Tiến Đạt',
          class: 'Điện tử 04 - K60',
          position: 'Thành viên',
          MSSV: '20150871'
        },
        {
          name: 'Dương Văn Khơi',
          class: 'Điện tử 04 - K60',
          position: 'Thành viên',
          MSSV: '20152033'
        },
        {
          name: 'Hoàng Đình Nam',
          class: 'Điện tử 10 - K60',
          position: 'Thành viên',
          MSSV: '20152535'
        },
      ]
    };
  }

  render() {
    const { dispatch } = this.props;
    const columns = [
      {
        Header: 'STT',
        id: 'stt',
        width: 50,
        Cell: ({ index }) => index + 1,
        filterable: false
      },
      {
        Header: 'Name',
        accessor: 'name',
        filterable: false
      },
      {
        Header: 'MSSV',
        accessor: 'MSSV',
        filterable: false
      },
      {
        Header: 'Class',
        accessor: 'class',
        filterable: false
      },
      {
        Header: 'Position',
        accessor: 'position',
        filterable: false
      },
    ];

    return (
      <div>
        <h1>Project TCQHVT - 20191</h1>
        <h3>Thông tin thành viên nhóm 05:</h3>
        <div className='text-center'>
          <Table
            data={this.state.members}
            columns={columns}
            className="react-table-custom"
            previousText="Trang trước"
            nextText="Trang tiếp theo"
            loadingText="Đang tải dữ liệu..."
            noDataText="Không có dữ liệu nào được tìm thấy"
            pageText="Trang"
            ofText="của"
            rowsText="Hàng"
            // Accessibility Labels
            pageJumpText="Nhảy tới trang"
            rowsSelectorText="Số hàng mỗi trang"
            //Genera
            pageSizeOptions={[10, 25, 50, 100, 200]}
            defaultPageSize={5}
            minRows={1}
          />
        </div>
      </div>
    );
  }
}

export default Dashboard;