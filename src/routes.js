import React from 'react';
const Dashboard = React.lazy(() => import('./views/Components/Pages/Dashboard'));
const MentorLayout = React.lazy(() => import('./views/Components/Pages/MentorLayout'));
const TutorialLayout = React.lazy(() => import('./views/Components/Pages/TutorialLayout'));
const SettingLayout = React.lazy(() => import('./views/Components/Pages/SettingLayout'));

const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/dashboard', name: 'Dashboard', component: Dashboard},
    { path: '/mentor', name: 'Mentor', component: MentorLayout },
    { path: '/tutorial', name: 'Tutorial', component: TutorialLayout },
    { path: '/setting', name: 'Setting', component: SettingLayout },
];

export default routes;