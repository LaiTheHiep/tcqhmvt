export default{
  items: [
    {
      name: 'Trang chủ',
      url: '/',
      icon: 'icon-speedometer',
    },
    {
      name: 'Project',
      url: '/mentor',
      icon: 'icon-layers',
    },
    {
      name: 'Hướng dẫn sử dụng',
      url: '/tutorial',
      icon: 'cui-task'
    },
    {
      name: 'Hướng dẫn cài đặt',
      url: '/setting',
      icon: 'cui-settings'
    }
  ]
};